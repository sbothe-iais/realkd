package de.unibonn.realkd.discoveryprocess;

import java.util.ArrayList;
import java.util.List;

import de.unibonn.realkd.patterns.Pattern;

/**
 * Contains the currently maintained patterns in a DiscoveryProcess. This
 * includes candidate patterns, result patterns, and deleted patterns. State is
 * altered by DiscoveryProcess.
 * 
 * @see DiscoveryProcess
 * 
 * @author mboley
 * 
 */
public class DiscoveryProcessState {

	private final List<Pattern> resultPatterns;
	private final List<Pattern> candidatePatterns;
	private final List<Pattern> discardedPatterns;
//	private boolean isFrozen;

	/*
	 * only to be called by DiscoveryProcess
	 */
	DiscoveryProcessState() {
		this.resultPatterns = new ArrayList<>();
		this.candidatePatterns = new ArrayList<>();
		this.discardedPatterns = new ArrayList<>();
//		this.isFrozen = false;
	}

	/*
	 * only to be called by DiscoveryProcess
	 */
	void setCandidateResultPatterns(List<Pattern> candidateResultPatterns) {
		this.candidatePatterns.clear();
		this.candidatePatterns.addAll(candidateResultPatterns);
	}

	/*
	 * only to be called by DiscoveryProcess
	 */
	synchronized void addResultPattern(Pattern pattern) {
//		if (!isFrozen()) {
			resultPatterns.add(pattern);
//		}
	}

	/*
	 * only to be called by DiscoveryProcess
	 */
	synchronized void removeResultPattern(Pattern pattern) {
//		if (!isFrozen()) {
			resultPatterns.remove(pattern);
//		}
	}

	/*
	 * only to be called by DiscoveryProcess
	 */
	synchronized void removeCandidatePattern(Pattern pattern) {
//		if (!isFrozen()) {
			candidatePatterns.remove(pattern);
//		}
	}

	/*
	 * only to be called by DiscoveryProcess
	 */
	synchronized void addDiscardedPattern(Pattern pattern) {
//		if (!isFrozen()) {
			discardedPatterns.add(pattern);
//		}
	}

	public synchronized List<Pattern> getCandidatePatterns() {
		return new ArrayList<>(candidatePatterns);
	}

	public synchronized boolean isInResults(Pattern p) {
		return resultPatterns.contains(p);
	}

	public synchronized boolean isInDiscarded(Pattern p) {
		return discardedPatterns.contains(p);
	}

	public synchronized boolean isInCandidates(Pattern p) {
		return candidatePatterns.contains(p);
	}

	/**
	 * Returns a new list of patterns in the result set in chronological order
	 * they got inserted.
	 * 
	 */
	public synchronized List<Pattern> getResultPatterns() {
		return new ArrayList<>(resultPatterns);
	}

	public synchronized List<Pattern> getDiscardedPatterns() {
		return new ArrayList<>(discardedPatterns);
	}
	
//	public void freeze() {
//		isFrozen = true;
//	}
//	public void unFreeze() {
//		isFrozen = false;
//	}
//	
//	public boolean isFrozen() {
//		return isFrozen;
//	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Candidates=");
		sb.append(candidatePatterns.size());
		sb.append(" Discarded=");
		sb.append(discardedPatterns.size());
		sb.append(" ResultPatterns=");
		sb.append(resultPatterns.size());
		return sb.toString();
	}
}