# realKD #

This file is part of the realKD library version 0.1 - a Java library 
for building applications that enable real users to discover real 
knowledge from real data. 

## Copying ##
The library is distributed freely under the MIT License (see 
LICENSE.txt). The original release is (c) 2014-15 by the contributors of
the realKD project (see CONTRIBUTORS). In addition to the LICENSE file, 
redistributions must also contain this README notice and all other text 
files referenced directly or indirectly in this file.

## Contributing ##
If you want to contribute to the realKD project make sure that you have
read and understood the most recent version of the contributors license
agreement (see CONTRIBUTORS_LICENSE_AGREEMENT). By issuing a pull request
or by any other form of intentional submission of a contribution you
signal your consent to this agreement.

## Copyright Notes ##
This distribution of realKD contains source files that are based
on libsvm (c) 2000-2014 by Chih-Chung Chang and Chih-Jen Lin
(see LICENSE_LIBSVM).

## Acknowledgement ##
The development of realKD was supported by the German Science
Foundation (DFG) under grant number 'GA 1615/2-1'