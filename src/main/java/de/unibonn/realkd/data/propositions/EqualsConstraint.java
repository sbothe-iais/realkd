/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.data.propositions;

import de.unibonn.realkd.data.propositions.Constraint;
import de.unibonn.realkd.data.propositions.DerivedConstraint;
import de.unibonn.realkd.data.propositions.EqualsConstraint;
import de.unibonn.realkd.data.propositions.GreaterThanConstraint;
import de.unibonn.realkd.data.propositions.LessThanConstraint;

public class EqualsConstraint<T> implements Constraint<T> {

	private T value;

	public EqualsConstraint(T value) {
		this.value = value;
	}

	@Override
	public boolean holds(T queryValue) {
		return queryValue.equals(this.value);
	}

	@Override
	public String getSuffixNotationName() {
		return "=" + value;
	}

	@Override
	public String getDescription() {
		return value.toString();
	}

	public T getComparisonValue() {
		return value;
	}

	@Override
	public boolean implies(Constraint<T> anotherConstraint) {
		if (anotherConstraint instanceof EqualsConstraint) {
			EqualsConstraint<T> anotherEqualsConstraint = (EqualsConstraint<T>) anotherConstraint;
			return value.equals(anotherEqualsConstraint.getComparisonValue());
		}
		if (anotherConstraint instanceof LessThanConstraint) {
			LessThanConstraint lessThanConstraint = (LessThanConstraint) anotherConstraint;
			if (value instanceof Comparable) {
				return lessThanConstraint.getComparisonValue().compareTo(
						(Comparable) value) > 0;
			} else {
				return false;
			}
		}
		if (anotherConstraint instanceof GreaterThanConstraint) {
			GreaterThanConstraint greaterThanConstraint = (GreaterThanConstraint) anotherConstraint;
			if (value instanceof Comparable) {
				return greaterThanConstraint.getComparisonValue().compareTo(
						(Comparable) value) < 0;
			} else {
				return false;
			}
		}
		if (anotherConstraint instanceof DerivedConstraint) {
			return ((DerivedConstraint<T>) anotherConstraint).impliedBy(this);
		}

		return false;
	}

	// @Override
	// public String getConstraintValue() {
	// return value;
	// }

}
