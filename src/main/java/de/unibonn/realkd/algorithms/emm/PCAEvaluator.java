/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.algorithms.emm;

import java.util.List;

import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.CategoricalAttribute;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;
import weka.attributeSelection.PrincipalComponents;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;

public class PCAEvaluator {

	private Instances transformedData;

	public PCAEvaluator(DataTable dataTable, List<Attribute> targetAttributes) {
		Instances instances = convertToInstances(dataTable, targetAttributes);
		PrincipalComponents pca = new PrincipalComponents();
		transformedData = null;
		try {
			pca.setCenterData(true);
			pca.buildEvaluator(instances);
			transformedData = pca.transformedData(instances);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public double getDevFirstDimension(int index) {
		return transformedData.instance(index).value(0);
	}

	public double getDevForDimension(int index, int dimension) {
		if (dimension >= transformedData.instance(index).numValues()) {
			return 0;
		}
		return transformedData.instance(index).value(dimension);
	}

	private static Instances convertToInstances(DataTable dataTable,
			List<Attribute> targetAttributes) {
		Instances instances = new Instances("converted",
				convertTargetAttributes(targetAttributes), dataTable.getSize());
		for (int i = 0; i < dataTable.getSize(); i++) {
			double[] values = new double[targetAttributes.size()];
			for (int j = 0; j < targetAttributes.size(); j++) {
				if (targetAttributes.get(j).isValueMissing(i)) {
					values[j] = ((MetricAttribute) targetAttributes.get(j))
							.getMedian();
				} else {
					values[j] = ((MetricAttribute) targetAttributes.get(j))
							.getValue(i);
				}
			}
			instances.add(new Instance(1., values));
		}
		return instances;
	}

	private static FastVector convertTargetAttributes(
			List<Attribute> targetAttributes) {
		FastVector attributes = new FastVector(targetAttributes.size());
		for (Attribute attribute : targetAttributes) {
			if (attribute instanceof CategoricalAttribute) {
				FastVector values = new FastVector();
				for (String value : ((CategoricalAttribute) attribute)
						.getCategories()) {
					values.addElement(value);
				}
				attributes.addElement(new weka.core.Attribute(attribute
						.getName(), values));
			} else {
				attributes.addElement(new weka.core.Attribute(attribute
						.getName()));
			}
		}
		return attributes;
	}
}
