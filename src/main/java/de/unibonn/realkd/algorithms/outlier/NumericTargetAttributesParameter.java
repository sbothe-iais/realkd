/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.algorithms.outlier;

import java.util.ArrayList;
import java.util.List;

import de.unibonn.realkd.algorithms.common.DataTableParameter;
import de.unibonn.realkd.common.parameter.AbstractNonEmptySubListParameter;
import de.unibonn.realkd.common.parameter.SubCollectionParameter;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;

public final class NumericTargetAttributesParameter extends
		AbstractNonEmptySubListParameter<Attribute> implements
		SubCollectionParameter<Attribute, List<Attribute>> {

	private static final String HINT = "Select at least one attribute";

	private static final String DESCRIPTION = "List of numeric attributes for which patterns should show special characteristics";

	private static final String NAME = "Numeric Target attributes";

	private final DataTableParameter dataTableParameter;

	public NumericTargetAttributesParameter(
			DataTableParameter dataTableParameter) {
		super(NAME, DESCRIPTION, List.class, null, HINT, dataTableParameter);
		this.dataTableParameter = dataTableParameter;
	}

	@Override
	public List<Attribute> getConcreteRange() {
		List<Attribute> result = new ArrayList<>();
		DataTable table = dataTableParameter.getCurrentValue();
		List<Attribute> attributes = dataTableParameter.getCurrentValue()
				.getAttributes();

		for (int i = 0; i < attributes.size(); ++i) {
			if (table.isNumeric(i)) {
				result.add(attributes.get(i));
			}
		}
		return result;
	}

	public DataTableParameter getDatatableParameter() {
		return dataTableParameter;
	}

}