/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.common.parameter;

import de.unibonn.realkd.common.logger.LogMessageType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static de.unibonn.realkd.common.logger.LogChannel.DEFAULT;

/**
 * Helper interface used in MiningParameter framework to provide abstract
 * implementations of the setByString-method of MiningParameter.
 * 
 * @author mboley
 * 
 */
public interface StringParser<T> {

	T parse(String... strings);

	static class TakeFirstStringParser implements StringParser<String> {

		@Override
		public String parse(String... strings) {
			if (strings.length == 0) {
				throw new IllegalArgumentException(
						"No Strings provided");
			}

			return strings[0];
		}
	}

	static TakeFirstStringParser TAKE_FIRST_STRING_PARSER = new TakeFirstStringParser();

	static class IntegerParser implements StringParser<Integer> {

		@Override
		public Integer parse(String... strings) {
			DEFAULT.log("mining_parameter",
					"parse strings " + Arrays.toString(strings),
					LogMessageType.DEBUG_MESSAGE);
			if (strings.length != 1) {
				throw new IllegalArgumentException(
						"Can only parse exactly one string");
			}
			return Integer.parseInt(strings[0]);
		}

		@Override
		public String toString() {
			return "INTEGER_PARSER";
		}

		private IntegerParser() {
			;
		}

	}

	static IntegerParser INTEGER_PARSER = new IntegerParser();

	static class DoubleParser implements StringParser<Double> {

		@Override
		public Double parse(String... strings) {
			DEFAULT.log("mining_parameter",
					"parse strings " + Arrays.toString(strings),
					LogMessageType.DEBUG_MESSAGE);
			if (strings.length != 1) {
				throw new IllegalArgumentException(
						"Can only parse exactly one string");
			}
			return Double.parseDouble(strings[0]);
		}

		@Override
		public String toString() {
			return "DOUBLE_PARSER";
		}

		private DoubleParser() {
			;
		}

	}

	static DoubleParser DOUBLE_PARSER = new DoubleParser();

	/**
	 * Parses toString representation of list of elements that are uniquely
	 * identifiable with respect to this representation. Objects are mutable:
	 * the underlying list can be reset.
	 */
	static class ListElementParser<T> implements StringParser<T> {

		private HashMap<String, T> map;

		@Override
		public T parse(String... strings) {
			DEFAULT.log("mining_parameter",
					"parse string " + Arrays.toString(strings),
					LogMessageType.DEBUG_MESSAGE);
			if (map == null) {
				throw new IllegalStateException("list not yet set");
			}
			if (strings.length != 1) {
				throw new IllegalArgumentException(
						"Can only parse exactly one string");
			}
			T result = map.get(strings[0]);
			if (result == null) {
				throw new IllegalArgumentException("cannot parse '"
						+ strings[0] + "' (not in list)");
			}
			DEFAULT.log("mining_parameter", "object found: " + result,
					LogMessageType.DEBUG_MESSAGE);
			return result;
		}

		@Override
		public String toString() {
			return "listElementParser";
		}

		public ListElementParser() {
			;
		}

		public void setList(List<T> list) {
			this.map = new HashMap<>(list.size());
			for (T element : list) {
				if (map.containsKey(element.toString())) {
					throw new IllegalArgumentException(
							"must give list elements with unique toString representation");
				}
				map.put(element.toString(), element);
			}
		}

	}

	static class SubListParser<T extends List> implements StringParser<T> {

		private HashMap<String, Object> map;

		@Override
		public T parse(String... strings) {
			DEFAULT.log("mining_parameter",
					"parse strings " + Arrays.toString(strings),
					LogMessageType.DEBUG_MESSAGE);
			if (map == null) {
				throw new IllegalStateException("list not yet set");
			}
			T result = (T) new ArrayList();
			for (String string : strings) {
				Object object = map.get(string);
				result.add(object);
				if (object == null) {
					throw new IllegalArgumentException("cannot parse " + string
							+ " (not in list)");
				}
			}
			DEFAULT.log("mining_parameter", "sublist parsed: " + result,
					LogMessageType.DEBUG_MESSAGE);
			return result;
		}

		@Override
		public String toString() {
			return "subListParser";
		}

		public void setList(@SuppressWarnings("rawtypes") List list) {
			this.map = new HashMap<>(list.size());
			for (Object element : list) {
				if (map.containsKey(element.toString())) {
					throw new IllegalArgumentException(
							"must give list elements with unique toString representation");
				}
				map.put(element.toString(), element);
			}
		}

	}

}
