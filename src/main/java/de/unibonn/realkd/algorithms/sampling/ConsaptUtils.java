/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.algorithms.sampling;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import mime.plain.PlainTransactionDB;
import mime.plain.weighting.PosNegDbInterface;
import mime.plain.weighting.PosNegTransactionDb;
import de.unibonn.realkd.algorithms.emm.PosNegDecider;
import de.unibonn.realkd.algorithms.emm.PosNegDecider.MultipleAttributesPosNegDecider;
import de.unibonn.realkd.algorithms.emm.PosNegDecider.PCAPosNegDecider;
import de.unibonn.realkd.algorithms.emm.PosNegDecider.SingleAttributePosNegDecider;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.propositions.PropositionalLogic;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;

public class ConsaptUtils {

	public static PlainTransactionDB createTransactionDBfromDataTable(
			PropositionalLogic propositionalLogic) {
		PlainTransactionDB transactionDB = new PlainTransactionDB();

		for (int i = 0; i < propositionalLogic.getSize(); i++) {
			List<String> transactionList = new ArrayList<>();
			for (Proposition proposition : propositionalLogic.getPropositions()) {
				if (proposition.holdsFor(i)) {
					transactionList.add(String.valueOf(proposition
							.getIndexInStore()));
				}
			}
			String[] transaction = new String[transactionList.size()];
			transactionList.toArray(transaction);
			transactionDB.addTransaction(transaction);
		}

		return transactionDB;
	}

	public static PosNegTransactionDb createPosNegDb(
			PropositionalLogic propLogic, Set<Attribute> targetAttributeSet,
			PosNegDecider decider) {
		PosNegTransactionDb db = new PosNegTransactionDb();
		for (int i = 0; i < propLogic.getSize(); i++) {

			// this should be implemented in the future:

			// do not create transaction for object i if at least one target
			// value is missing
			// if (dataTable.atLeastOneAttributeValueMissingFor(i, new
			// ArrayList<>(targetAttributeSet))) {
			// continue;
			// }
			List<String> transactionList = new ArrayList<>();
			for (Proposition proposition : propLogic.getPropositions()) {
				if (targetAttributeSet.contains(proposition.getAttribute())
						|| propLogic
								.getDatatable()
								.getAttributeGroupsStore()
								.isPartOfMacroAttributeWithAtLeastOneOf(
										proposition.getAttribute(),
										targetAttributeSet)) {
					continue;
				}
				if (proposition.holdsFor(i)) {
					transactionList.add(String.valueOf(proposition
							.getIndexInStore()));
				}
			}
			if (decider.isPos(i)) {
				db.addTransaction(transactionList.toArray(new String[] {}),
						true);
			} else {
				db.addTransaction(transactionList.toArray(new String[] {}),
						false);
			}
		}
		return db;
	}

	public static PosNegTransactionDb createPosNegTransactionDBByFirstAttribute(
			PropositionalLogic probLogic, List<Attribute> targetAttributes) {
		Set<Attribute> targetAttributeSet = newHashSet(targetAttributes); // .get(0),
																			// targetAttributes.get(1));
		//
		PosNegDecider decider = new SingleAttributePosNegDecider(
				targetAttributes.get(0));

		return createPosNegDb(probLogic, targetAttributeSet, decider);
	}

	public static PosNegDbInterface createPosNegTransactionDBByFirstTwoAttributes(
			PropositionalLogic probLogic, List<Attribute> targetAttributes) {
		Set<Attribute> targetAttributeSet = newHashSet(targetAttributes);

		PosNegDecider decider = new MultipleAttributesPosNegDecider(
				newArrayList(targetAttributes.get(0), targetAttributes.get(1)));

		return createPosNegDb(probLogic, targetAttributeSet, decider);
	}

	public static PosNegDbInterface createPosNegTransactionDBUsingPCA(
			PropositionalLogic probLogic, List<Attribute> targetAttributes) {
		Set<Attribute> targetAttributeSet = newHashSet(targetAttributes);

		PCAPosNegDecider decider = new PCAPosNegDecider(
				probLogic.getDatatable(), targetAttributes);

		return createPosNegDb(probLogic, targetAttributeSet, decider);
	}
}
