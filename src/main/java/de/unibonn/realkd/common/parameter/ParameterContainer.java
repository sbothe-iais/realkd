/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.realkd.common.parameter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * Interface of objects that contain mutable fields (parameters) that are
 * supposed to be manipulated at runtime and for which a consistency check and
 * documentation is provided. Parameters within a container are ordered to take
 * into account parameter dependencies: if a parameter x dependents on another
 * parameter y then y must precede x in the order.
 * </p>
 * 
 * <p>
 * Values of parameters can again be parameter containers in some cases. Hence
 * some methods work in recursive fashion.
 * </p>
 * 
 * @see {@link Parameter}, {@link DependentParameter}
 * 
 * @author bjacobs, mboley
 * 
 */
public interface ParameterContainer {

	/**
	 * Finds reference to parameter with specified name within container. This
	 * includes recursively nested parameters that are currently available as
	 * parameters of values of parameters. Search proceeds in breadth-first
	 * order. Consequently, top level parameters hide parameters of nested
	 * parameters (and so on) with the same name.
	 * 
	 * @return the parameter with the specified name.
	 * @throws IllegalArgumentException
	 *             if no such parameter present.
	 */
	public Parameter<?> findParameterByName(String name);

	/**
	 * @return List of parameters in an order that is compatible with parameter
	 *         dependencies (see {@link DependentParameter}). Parameters can
	 *         have mixed type arguments.
	 */
	@SuppressWarnings("rawtypes")
	public List<Parameter> getTopLevelParameters();

	/**
	 * 
	 * @return List of all parameters resulting from depth first traversal
	 * 
	 */
	@SuppressWarnings("rawtypes")
	public default List<Parameter> getAllParameters() {
		List<Parameter> result = new ArrayList<Parameter>();
		for (Parameter<?> parameter : getTopLevelParameters()) {
			if (result.contains(parameter)) {
				throw new IllegalStateException(
						"cyclic parameter containment in container " + this
								+ " involving " + parameter.getName());
			}
			result.add(parameter);
			if (parameter.getCurrentValue() instanceof ParameterContainer) {
				result.addAll(((ParameterContainer) parameter.getCurrentValue())
						.getAllParameters());
			}
		}
		return result;
	}

	/**
	 * Convenience method that returns true if and only if the value of all
	 * contained parameters is valid.
	 * 
	 */
	public boolean isStateValid();

	/**
	 * <p>
	 * Recursively sets all parameters specified in key set of map to the values
	 * specified by value map. Parameters are set in the order compatible with
	 * the order of parameters returned by {@link #getTopLevelParameters()}. In
	 * addition, depth first order is applied in case nested parameters are
	 * present, i.e., parameters of values of other parameters. This way a
	 * parameter value can first be completely transfered to a valid state
	 * before potentially dependent parameters are set.
	 * </p>
	 * <p>
	 * Catches exception raised due to illegal string values (may issue
	 * warning).
	 * </p>
	 * 
	 * @param nameValueMap
	 */
	public void passValuesToParameters(Map<String, String[]> nameValueMap);

	/**
	 * <p>
	 * Same as {@link #passValuesToParameters} but removes those key value pairs
	 * that have been used from input map. Can be used by clients in order to
	 * determine which parameters could be passed and which not.
	 * </p>
	 * <p>
	 * WARNING: this modifies the input argument.
	 * </p>
	 * 
	 * @param crossOutMap
	 */
	public void unloadMapValuesToParameters(
			final Map<String, String[]> crossOutMap);

}
