package de.unibonn.realkd.data.table;

import static org.junit.Assert.*;

import org.junit.Test;

import de.unibonn.realkd.data.table.DataFormatException;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.DataTableFromCSVFileBuilder;

public class DataTableFromCSVFileBuilderTest {

	public static DataTableFromCSVFileBuilder builder = new DataTableFromCSVFileBuilder()
			.setDataCSVFilename("testdata//germany//data.txt")
			.setAttributeMetadataCSVFilename(
					"testdata//germany//attributes.txt")
			.setAttributeGroupCSVFilename("testdata//germany//groups.txt");

	private DataTable loadedDatatable;

	@Test
	public void testBuild() throws DataFormatException {
		this.loadedDatatable = builder.build();
		assertNotNull(loadedDatatable);
	}

}
