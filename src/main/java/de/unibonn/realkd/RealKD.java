/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-15 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */

package de.unibonn.realkd;

/**
 * RealKD is a general purpose pattern discovery library for discovering real
 * knowledge from real data for real users.
 * 
 * @author mboley, bjacobs, bkang, ptokmakov, smoens, sbothe, eefendiyev,
 *         vdzyuba
 * 
 */
public class RealKD {

	private static final String NAME = "realKD";

	private static final String COPYRIGHT = "(c) 2014-15 by the Contributors of the realKD project";

	private static String VERSION = "0.1";

	public static String getName() {
		return NAME;
	}
	
	public static String getVersion() {
		return VERSION;
	}

	public static void main(String[] args) {
		System.out.println(NAME + " " + VERSION + " " + COPYRIGHT);

		new CommandLineExecutor().run(args);
	}


}
